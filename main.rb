puts 'ЗАДАНИЕ №2'
string = IO.read('test2.txt')
puts 'По возрастанию: ' + string.split(',').map{|x| x.to_i}.sort.join(', ')
puts 'По убыванию: ' + string.split(',').map{|x| x.to_i}.sort {|x,y| y <=> x}.inspect

puts 'ЗАДАНИЕ №3'
def factorial (value)
  begin   
    integerValue = Integer(value) # на случай если ввели не целое число
  rescue
    return 'ошибка'
  end
    factorial = 1
    if integerValue > 0 # на случай если ввели не положительное число
        for i in 1..integerValue
            factorial *= i
        end
        factorial
    else
        'ошибка'
    end
end
value = 20
puts "Факториал от #{value.inspect} равен #{factorial(value).inspect}."